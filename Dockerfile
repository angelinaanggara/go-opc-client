ARG go_version=1.11.1
ARG alpine_version=3.8

FROM golang:${go_version}-alpine${alpine_version}

# Get gcc
RUN apk update && apk upgrade && apk add --no-cache build-base git

ENV REPO_DIR $GOPATH/src/gitlab.com/chiungyu/go-opc-client

# Copy modified open62541 0.3-rc2: UA_DataValue bit encoding removed as it's not cgo friendly.
COPY ./open62541 ${REPO_DIR}/open62541

# Build libopen62541
WORKDIR ${REPO_DIR}/open62541
RUN gcc -c open62541.c
RUN ar cr libopen62541.a open62541.o

# Add open62541 to gcc header path and static library path.
ENV CPATH="${REPO_DIR}/open62541:${CPATH}"
ENV LIBRARY_PATH="${REPO_DIR}/open62541:${LIBRARY_PATH}"

ENV GO111MODULE on

COPY ./opc ${REPO_DIR}/opc

# Set path for development.
WORKDIR $GOPATH/src

CMD ["/bin/sh"]
