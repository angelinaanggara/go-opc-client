package opc

/*
#cgo LDFLAGS: -lopen62541
#include "open62541.h"
*/
import "C"

import (
	"errors"
	"fmt"
	"unsafe"
	//API "path/to/project/callAPIService"
)

// Client is a wrapper for open62541 client connection.
type Client struct {
	client *C.UA_Client
}

// NewClient returns a new Client connected to the specified endpoint.
func NewClient(endpoint string) (*Client, error) {
	client := C.UA_Client_new(C.UA_ClientConfig_default)

	endpointString := C.CString(endpoint)
	defer C.free(unsafe.Pointer(endpointString))
	retval := C.UA_Client_connect(client, endpointString)
	if retval != C.UA_STATUSCODE_GOOD {
		fmt.Println(retval)
		return nil, fmt.Errorf("failed to connect: %d", retval)
	}
	return &Client{client}, nil
}

// Close deletes the underlying client.
func (c *Client) Close() {
	C.UA_Client_delete(c.client)
}

// DataType for read/write.
type DataType int

// DataType for read/write.
const (
	INT16 DataType = iota
	INT32
	UINT16
	UINT32
	STRING
	BOOLEAN
	//FLOAT
	DOUBLE
)

// DataValue parsed from OPC server data value.
type DataValue struct {
	Error      error
	Status     uint32
	SourceTime int64 // unix-nano, source accurate downto 100ns (picoseconds ignored), zero if unavailable
	ServerTime int64 // ditto.
	Value      interface{}
}

func unixNano(t C.UA_DateTime) int64 {
	return 100 * (int64(t) - C.UA_DATETIME_UNIX_EPOCH)
}

func parseDataValue(r *C.UA_DataValue, expectedType DataType) *DataValue {
	result := &DataValue{}
	if r == nil {
		result.Error = errors.New("nil result")
		return result
	}
	if r.hasStatus && r.status != C.UA_STATUSCODE_GOOD {
		result.Error = errors.New("bad status code")
		result.Status = uint32(r.status)
		return result
	}
	if !r.hasValue || C.UA_Variant_isEmpty(&r.value) {
		result.Error = errors.New("result contains no value")
		return result
	}

	if r.hasSourceTimestamp {
		result.SourceTime = unixNano(r.sourceTimestamp) // ignore sourcePicoseconds
	}
	if r.hasServerTimestamp {
		result.ServerTime = unixNano(r.serverTimestamp) // ignore serverPicoseconds
	}

	// TODO: share logic.
	switch expectedType {
	case INT16:
		if r.value._type != &(C.UA_TYPES[C.UA_TYPES_INT16]) {
			result.Error = errors.New("unexpected result data type")
		} else if C.UA_Variant_isScalar(&r.value) {
			result.Value = *(*int16)(r.value.data)
		} else { // isArray
			rp := uintptr(r.value.data)
			offset := unsafe.Sizeof(*(*C.UA_Int16)(r.value.data))

			len := int(r.value.arrayLength)
			arr := make([]int16, len)
			for i := 0; i < len; i++ {
				arr[i] = *(*int16)(unsafe.Pointer(rp))
				rp = rp + offset
			}
			result.Value = arr
		}
	case INT32:
		if r.value._type != &(C.UA_TYPES[C.UA_TYPES_INT32]) {
			result.Error = errors.New("unexpected result data type")
		} else if C.UA_Variant_isScalar(&r.value) {
			result.Value = *(*int32)(r.value.data)
		} else { // isArray
			rp := uintptr(r.value.data)
			offset := unsafe.Sizeof(*(*C.UA_Int32)(r.value.data))

			len := int(r.value.arrayLength)
			arr := make([]int32, len)
			for i := 0; i < len; i++ {
				arr[i] = *(*int32)(unsafe.Pointer(rp))
				rp = rp + offset
			}
			result.Value = arr
		}
	case UINT16:
		if r.value._type != &(C.UA_TYPES[C.UA_TYPES_UINT16]) {
			result.Error = errors.New("unexpected result data type")
		} else if C.UA_Variant_isScalar(&r.value) {
			result.Value = *(*uint16)(r.value.data)
		} else { // isArray
			rp := uintptr(r.value.data)
			offset := unsafe.Sizeof(*(*C.UA_UInt16)(r.value.data))

			len := int(r.value.arrayLength)
			arr := make([]uint16, len)
			for i := 0; i < len; i++ {
				arr[i] = *(*uint16)(unsafe.Pointer(rp))
				rp = rp + offset
			}
			result.Value = arr
		}
	case UINT32:
		if r.value._type != &(C.UA_TYPES[C.UA_TYPES_UINT32]) {
			result.Error = errors.New("unexpected result data type")
		} else if C.UA_Variant_isScalar(&r.value) {
			result.Value = *(*uint32)(r.value.data)
		} else { // isArray
			rp := uintptr(r.value.data)
			offset := unsafe.Sizeof(*(*C.UA_UInt32)(r.value.data))

			len := int(r.value.arrayLength)
			arr := make([]uint32, len)
			for i := 0; i < len; i++ {
				arr[i] = *(*uint32)(unsafe.Pointer(rp))
				rp = rp + offset
			}
			result.Value = arr
		}
	case STRING:
		if r.value._type != &(C.UA_TYPES[C.UA_TYPES_STRING]) {
			result.Error = errors.New("unexpected result data type")
		} else if C.UA_Variant_isScalar(&r.value) {
			s := (*C.UA_String)(r.value.data)
			result.Value = C.GoStringN((*C.char)(unsafe.Pointer(s.data)), C.int(s.length))
		} else { // isArray
			rp := uintptr(r.value.data)
			offset := unsafe.Sizeof(*(*C.UA_String)(r.value.data))

			len := int(r.value.arrayLength)
			arr := make([]string, len)
			for i := 0; i < len; i++ {
				s := (*C.UA_String)(unsafe.Pointer(rp))
				arr[i] = C.GoStringN((*C.char)(unsafe.Pointer(s.data)), C.int(s.length))
				rp = rp + offset
			}
			result.Value = arr
		}
	case BOOLEAN:
		if r.value._type != &(C.UA_TYPES[C.UA_TYPES_BOOLEAN]) {
			result.Error = errors.New("unexpected result data type")
		} else if C.UA_Variant_isScalar(&r.value) {
			result.Value = *(*bool)(r.value.data)
		} else { // isArray
			rp := uintptr(r.value.data)
			offset := unsafe.Sizeof(*(*C.UA_Boolean)(r.value.data))

			len := int(r.value.arrayLength)
			arr := make([]bool, len)
			for i := 0; i < len; i++ {
				arr[i] = *(*bool)(unsafe.Pointer(rp))
				rp = rp + offset
			}
			result.Value = arr
		}
	case DOUBLE:
		if r.value._type != &(C.UA_TYPES[C.UA_TYPES_DOUBLE]) {
			result.Error = errors.New("unexpected result data type")
		} else if C.UA_Variant_isScalar(&r.value) {
			result.Value = *(*float64)(r.value.data)
		} else { // isArray
			rp := uintptr(r.value.data)
			offset := unsafe.Sizeof(*(*C.UA_Double)(r.value.data))

			len := int(r.value.arrayLength)
			arr := make([]float64, len)
			for i := 0; i < len; i++ {
				arr[i] = *(*float64)(unsafe.Pointer(rp))
				rp = rp + offset
			}
			result.Value = arr
		}
	default:
		result.Error = errors.New("unknown result data type") // Paranoid
	}

	return result
}

// IndexRange for array read/write. Zero-indexed, both First and Last included.
type IndexRange struct {
	First, Last uint32
}

func (r *IndexRange) String() string {
	if r.First == r.Last {
		return fmt.Sprintf("%d", r.First)
	}
	return fmt.Sprintf("%d:%d", r.First, r.Last)
}

// NodeToRead contains the specification for a read node.
type NodeToRead struct {
	NsIndex  uint
	TagName  string
	ValType  DataType
	IdxRange *IndexRange
}

// ReadValues reads the nodes from OPC server and returns parsed DataValue objects.
func ReadValues(client *Client, nodes []*NodeToRead) ([]*DataValue, error) {
	n := len(nodes)

	if n <= 0 {
		return nil, errors.New("no nodes to read")
	}
	for i, node := range nodes {
		if node == nil {
			return nil, fmt.Errorf("nil node #%d", i)
		}
	}

	// Construct the UA_ReadValueId array
	uaNodes := make([]C.UA_ReadValueId, len(nodes)) // go zero-initializes objects
	for i, node := range nodes {
		tagString := C.CString(node.TagName)
		// defer C.free(unsafe.Pointer(tagString)) taken cared of by UA_ReadValueId_deleteMembers

		uaNodes[i].attributeId = C.UA_ATTRIBUTEID_VALUE
		uaNodes[i].nodeId = C.UA_NODEID_STRING(C.ushort(node.NsIndex), tagString)

		if node.IdxRange != nil {
			rangeString := C.CString(node.IdxRange.String())
			// defer C.free(unsafe.Pointer(rangeString)) taken cared of by UA_ReadValueId_deleteMembers
			uaNodes[i].indexRange = C.UA_STRING(rangeString)
		}
		defer C.UA_ReadValueId_deleteMembers(&uaNodes[i]) // clean up when exiting ReadValues function
	}

	var req C.UA_ReadRequest // go zero-initializes objects
	req.nodesToRead = &uaNodes[0]
	req.nodesToReadSize = C.ulong(len(nodes))

	resp := C.UA_Client_Service_read(client.client, req)
	defer C.UA_ReadResponse_deleteMembers(&resp)

	if resp.responseHeader.serviceResult != C.UA_STATUSCODE_GOOD {
		return nil, fmt.Errorf("read request failed with code: %d", resp.responseHeader.serviceResult)
	}
	if resp.resultsSize != C.ulong(n) {
		return nil, fmt.Errorf("wrong number of results returned: %d expected, %d returned", n, resp.resultsSize)
	}

	rp := uintptr(unsafe.Pointer(resp.results))
	offset := unsafe.Sizeof(*(resp.results))
	results := make([]*DataValue, n)
	for i, node := range nodes {
		r := (*C.UA_DataValue)(unsafe.Pointer(rp))
		results[i] = parseDataValue(r, node.ValType)
		rp = rp + offset
	}
	return results, nil
}

// NodeToWrite contains the specification for a write node.
type NodeToWrite struct {
	NsIndex  uint
	TagName  string
	ValType  DataType
	IdxRange *IndexRange
	ValData  interface{}
}

func cDataCopy(v interface{}, expectedType DataType, idxRange *IndexRange) (unsafe.Pointer, *C.UA_DataType, uint32, error) {
	var arrLen uint32
	isScalar := idxRange == nil
	if !isScalar {
		if idxRange.Last < idxRange.First {
			return nil, nil, 0, fmt.Errorf("bad index range %s", idxRange)
		}
		arrLen = idxRange.Last + 1 - idxRange.First
	}

	switch expectedType {
	case INT16:
		if isScalar {
			data, ok := v.(int16)
			if !ok {
				return nil, nil, 0, errors.New("unexpected data type")
			}
			copy := C.UA_Int16_new()
			*copy = C.UA_Int16(data)
			return unsafe.Pointer(copy), &C.UA_TYPES[C.UA_TYPES_INT16], 0, nil
		}
		data, ok := v.([]int16)
		if !ok {
			return nil, nil, 0, errors.New("unexpected data type")
		}
		if len(data) != int(arrLen) {
			return nil, nil, 0, errors.New("unexpected data array size")
		}
		copy := C.UA_Array_new(C.ulong(arrLen), &(C.UA_TYPES[C.UA_TYPES_INT16]))
		rp := uintptr(copy)
		offset := unsafe.Sizeof(*(*C.UA_Int16)(copy))
		for _, v := range data {
			*(*C.UA_Int16)(unsafe.Pointer(rp)) = C.UA_Int16(v)
			rp = rp + offset
		}
		return unsafe.Pointer(copy), &C.UA_TYPES[C.UA_TYPES_INT16], arrLen, nil
	case INT32:
		if isScalar {
			data, ok := v.(int32)
			if !ok {
				return nil, nil, 0, errors.New("unexpected data type")
			}
			copy := C.UA_Int32_new()
			*copy = C.UA_Int32(data)
			return unsafe.Pointer(copy), &C.UA_TYPES[C.UA_TYPES_INT32], 0, nil
		}
		data, ok := v.([]int32)
		if !ok {
			return nil, nil, 0, errors.New("unexpected data type")
		}
		if len(data) != int(arrLen) {
			return nil, nil, 0, errors.New("unexpected data array size")
		}
		copy := C.UA_Array_new(C.ulong(arrLen), &(C.UA_TYPES[C.UA_TYPES_INT32]))
		rp := uintptr(copy)
		offset := unsafe.Sizeof(*(*C.UA_Int32)(copy))
		for _, v := range data {
			*(*C.UA_Int32)(unsafe.Pointer(rp)) = C.UA_Int32(v)
			rp = rp + offset
		}
		return unsafe.Pointer(copy), &C.UA_TYPES[C.UA_TYPES_INT32], arrLen, nil
	case UINT16:
		if isScalar {
			data, ok := v.(uint16)
			if !ok {
				return nil, nil, 0, errors.New("unexpected data type")
			}
			copy := C.UA_UInt16_new()
			*copy = C.UA_UInt16(data)
			return unsafe.Pointer(copy), &C.UA_TYPES[C.UA_TYPES_UINT16], 0, nil
		}
		data, ok := v.([]uint16)
		if !ok {
			return nil, nil, 0, errors.New("unexpected data type")
		}
		if len(data) != int(arrLen) {
			return nil, nil, 0, errors.New("unexpected data array size")
		}
		copy := C.UA_Array_new(C.ulong(arrLen), &(C.UA_TYPES[C.UA_TYPES_UINT16]))
		rp := uintptr(copy)
		offset := unsafe.Sizeof(*(*C.UA_UInt16)(copy))
		for _, v := range data {
			*(*C.UA_UInt16)(unsafe.Pointer(rp)) = C.UA_UInt16(v)
			rp = rp + offset
		}
		return unsafe.Pointer(copy), &C.UA_TYPES[C.UA_TYPES_UINT16], arrLen, nil
	case UINT32:
		if isScalar {
			data, ok := v.(uint32)
			if !ok {
				return nil, nil, 0, errors.New("unexpected data type")
			}
			copy := C.UA_UInt32_new()
			*copy = C.UA_UInt32(data)
			return unsafe.Pointer(copy), &C.UA_TYPES[C.UA_TYPES_UINT32], 0, nil
		}
		data, ok := v.([]uint32)
		if !ok {
			return nil, nil, 0, errors.New("unexpected data type")
		}
		if len(data) != int(arrLen) {
			return nil, nil, 0, errors.New("unexpected data array size")
		}
		copy := C.UA_Array_new(C.ulong(arrLen), &(C.UA_TYPES[C.UA_TYPES_UINT32]))
		rp := uintptr(copy)
		offset := unsafe.Sizeof(*(*C.UA_UInt32)(copy))
		for _, v := range data {
			*(*C.UA_UInt32)(unsafe.Pointer(rp)) = C.UA_UInt32(v)
			rp = rp + offset
		}
		return unsafe.Pointer(copy), &C.UA_TYPES[C.UA_TYPES_UINT32], arrLen, nil
	case STRING:
		if isScalar {
			data, ok := v.(string)
			if !ok {
				return nil, nil, 0, errors.New("unexpected data type")
			}
			copy := C.UA_String_new()
			cstr := C.CString(data)
			*copy = C.UA_STRING(cstr)
			return unsafe.Pointer(copy), &C.UA_TYPES[C.UA_TYPES_STRING], 0, nil
		}
		data, ok := v.([]string)
		if !ok {
			return nil, nil, 0, errors.New("unexpected data type")
		}
		if len(data) != int(arrLen) {
			return nil, nil, 0, errors.New("unexpected data array size")
		}
		copy := C.UA_Array_new(C.ulong(arrLen), &(C.UA_TYPES[C.UA_TYPES_STRING]))
		rp := uintptr(copy)
		offset := unsafe.Sizeof(*(*C.UA_String)(copy))
		for _, v := range data {
			cstr := C.CString(v)
			*(*C.UA_String)(unsafe.Pointer(rp)) = C.UA_STRING(cstr)
			rp = rp + offset
		}
		return unsafe.Pointer(copy), &C.UA_TYPES[C.UA_TYPES_STRING], arrLen, nil
	case BOOLEAN:
		if isScalar {
			data, ok := v.(bool)
			if !ok {
				return nil, nil, 0, errors.New("unexpected data type")
			}
			copy := C.UA_Boolean_new()
			*copy = C.UA_Boolean(data)
			return unsafe.Pointer(copy), &C.UA_TYPES[C.UA_TYPES_BOOLEAN], 0, nil
		}
		data, ok := v.([]bool)
		if !ok {
			return nil, nil, 0, errors.New("unexpected data type")
		}
		if len(data) != int(arrLen) {
			return nil, nil, 0, errors.New("unexpected data array size")
		}
		copy := C.UA_Array_new(C.ulong(arrLen), &(C.UA_TYPES[C.UA_TYPES_BOOLEAN]))
		rp := uintptr(copy)
		offset := unsafe.Sizeof(*(*C.UA_Boolean)(copy))
		for _, v := range data {
			*(*C.UA_Boolean)(unsafe.Pointer(rp)) = C.UA_Boolean(v)
			rp = rp + offset
		}
		return unsafe.Pointer(copy), &C.UA_TYPES[C.UA_TYPES_BOOLEAN], arrLen, nil
	case DOUBLE:
		if isScalar {
			data, ok := v.(float64)
			if !ok {
				return nil, nil, 0, errors.New("unexpected data type")
			}
			copy := C.UA_Double_new()
			*copy = C.UA_Double(data)
			return unsafe.Pointer(copy), &C.UA_TYPES[C.UA_TYPES_DOUBLE], 0, nil
		}
		data, ok := v.([]float64)
		if !ok {
			return nil, nil, 0, errors.New("unexpected data type")
		}
		if len(data) != int(arrLen) {
			return nil, nil, 0, errors.New("unexpected data array size")
		}
		copy := C.UA_Array_new(C.ulong(arrLen), &(C.UA_TYPES[C.UA_TYPES_DOUBLE]))
		rp := uintptr(copy)
		offset := unsafe.Sizeof(*(*C.UA_Double)(copy))
		for _, v := range data {
			*(*C.UA_Double)(unsafe.Pointer(rp)) = C.UA_Double(v)
			rp = rp + offset
		}
		return unsafe.Pointer(copy), &C.UA_TYPES[C.UA_TYPES_DOUBLE], arrLen, nil
	default: // Paranoid
		return nil, nil, 0, errors.New("unknown data type")
	}
}

// WriteValues writes the nodes to OPCServer and returns the response status codes.
func WriteValues(client *Client, nodes []*NodeToWrite) ([]uint32, error) {
	n := len(nodes)

	if n <= 0 {
		return nil, errors.New("no nodes to write")
	}
	for i, node := range nodes {
		if node == nil {
			return nil, fmt.Errorf("nil node #%d", i)
		}
	}

	// Create and populate the request with the cSpecs
	uaNodes := make([]C.UA_WriteValue, n) // go zero-initializes objects
	for i, node := range nodes {
		uaNodes[i].attributeId = C.UA_ATTRIBUTEID_VALUE
		uaNodes[i].value.hasValue = true

		data, uaType, arrLen, err := cDataCopy(node.ValData, node.ValType, node.IdxRange)
		if err != nil {
			return nil, fmt.Errorf("failed to copy data from node #%d: %s", i, err)
		}

		uaNodes[i].value.value._type = uaType
		uaNodes[i].value.value.arrayLength = C.ulong(arrLen)
		uaNodes[i].value.value.data = data
		uaNodes[i].value.value.storageType = C.UA_VARIANT_DATA // UA_WriteValue will be responsible for cleaning up, see deferred UA_WriteValue_deleteMembers below

		tagString := C.CString(node.TagName)
		uaNodes[i].nodeId = C.UA_NODEID_STRING(C.ushort(node.NsIndex), tagString)

		defer C.UA_WriteValue_deleteMembers(&uaNodes[i]) // clean up tag string and data variant when exiting WriteValues
	}

	var req C.UA_WriteRequest // go zero-initializes objects
	req.nodesToWrite = &uaNodes[0]
	req.nodesToWriteSize = C.ulong(n)

	resp := C.UA_Client_Service_write(client.client, req)
	defer C.UA_WriteResponse_deleteMembers(&resp)

	if resp.responseHeader.serviceResult != C.UA_STATUSCODE_GOOD {
		return nil, fmt.Errorf("write request failed with code: %d", resp.responseHeader.serviceResult)
	}
	if resp.resultsSize != C.ulong(n) {
		return nil, fmt.Errorf("wrong number of results returned: %d expected, %d returned", n, resp.resultsSize)
	}

	rp := uintptr(unsafe.Pointer(resp.results))
	offset := unsafe.Sizeof(*(resp.results))
	results := make([]uint32, n)
	for i := 0; i < n; i++ {
		results[i] = *(*uint32)(unsafe.Pointer(rp))
		rp = rp + offset
	}

	return results, nil
}
